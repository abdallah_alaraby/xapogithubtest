# XapoGithubTest
by **Abdallah Alaraby**



## Structure
The structure used in this test is based on MVVM (Model-View-ViewModel) design pattern. I decided to use it because of the following aspects:

* Apply the Separation of concerns principle
* Handle configuration changes easily
* And because using any more complex architecture would be over-structuring the code.

----
## Tools & Technologies
1. Kotlin
2. Retrofit2
3. RxJava2
4. GSON
5. ViewModel
6. LiveData

----
## Other
Total Time Spent: 5 hours approximately