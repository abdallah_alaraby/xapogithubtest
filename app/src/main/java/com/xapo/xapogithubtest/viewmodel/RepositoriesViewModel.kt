package com.xapo.xapogithubtest.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.xapo.xapogithubtest.model.ApiError
import com.xapo.xapogithubtest.model.ListReposCloudImpl
import com.xapo.xapogithubtest.model.RepositoryItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.io.IOException

class RepositoriesViewModel : ViewModel() {
    val repositoriesListLiveData = MutableLiveData<List<RepositoryItem>?>()
    val errorLiveData = MutableLiveData<ApiError>()
    var isRequested = false
    private val mCloud = ListReposCloudImpl()
    private var mReposDisposable: Disposable? = null

    fun getRepositoriesList() {
        mReposDisposable = mCloud.getRepositories("android",
                "stars",
                "desc")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ items ->
                    repositoriesListLiveData.postValue(items)
                }, { throwable ->
                    when (throwable) {
                        is HttpException ->
                            errorLiveData.postValue(ApiError(ApiError.ApiErrorType.API, throwable.response().message()))
                        is IOException -> errorLiveData.postValue(ApiError(ApiError.ApiErrorType.NETWORK))
                        else -> {
                            errorLiveData.postValue(ApiError(ApiError.ApiErrorType.UNEXPECTED))
                        }
                    }
                    repositoriesListLiveData.postValue(null)
                })
    }

    override fun onCleared() {
        if (!mReposDisposable!!.isDisposed) {
            mReposDisposable!!.dispose()
        }
        super.onCleared()
    }
}