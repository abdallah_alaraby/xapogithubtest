package com.xapo.xapogithubtest.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.xapo.xapogithubtest.R
import com.xapo.xapogithubtest.model.RepositoryItem

class RepositoriesAdapter : RecyclerView.Adapter<RepositoriesAdapter.ViewHolder>() {
    private var repos = ArrayList<RepositoryItem>()
    var onRepositoryClickListener: OnRepositoryClickListener? = null

    fun setRepos(repos: List<RepositoryItem>?) {
        this.repos.clear()
        if (repos != null) {
            this.repos.addAll(repos)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_repository, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.repos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repo = repos[position]
        holder.textRepoName!!.text = repo.name
        holder.textRepoDescription!!.text = repo.description
        holder.textStars!!.text = repo.stargazers_count.toString()
        holder.textForks!!.text = repo.forks_count.toString()
        holder.itemView.setOnClickListener({
            if (onRepositoryClickListener != null) {
                onRepositoryClickListener?.onRepositoryClick(repo)
            }
        })
    }

    interface OnRepositoryClickListener {
        fun onRepositoryClick(repositoryItem: RepositoryItem)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textRepoName: TextView? = null
        var textRepoDescription: TextView? = null
        var textStars: TextView? = null
        var textForks: TextView? = null

        init {
            textRepoName = itemView.findViewById(R.id.text_repo_name)
            textRepoDescription = itemView.findViewById(R.id.text_repo_description)
            textStars = itemView.findViewById(R.id.text_stars)
            textForks = itemView.findViewById(R.id.text_forks)
        }
    }
}