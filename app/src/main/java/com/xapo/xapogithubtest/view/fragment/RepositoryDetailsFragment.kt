package com.xapo.xapogithubtest.view.fragment

import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Html
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.xapo.xapogithubtest.R
import com.xapo.xapogithubtest.model.RepositoryItem
import kotlinx.android.synthetic.main.fragment_repository_details.*

class RepositoryDetailsFragment : Fragment() {

    companion object {
        const val ARGUMENT_REPOSITORY = "repository"
        fun newInstance(repo: RepositoryItem): RepositoryDetailsFragment {
            val fragment = RepositoryDetailsFragment()
            val args = Bundle()
            args.putParcelable(ARGUMENT_REPOSITORY, repo)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_repository_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val repo: RepositoryItem = arguments!!.getParcelable(ARGUMENT_REPOSITORY)
        textRepoName.text = repo.name
        textRepoDescription.text = repo.description
        textStars.text = repo.stargazers_count.toString()
        textForks.text = repo.forks_count.toString()
        textRepoLanguage.text = repo.language
        if (repo.owner != null) {
            textRepoOwner.text = getLink(repo.owner.html_url, repo.owner.login)
            textRepoOwner.movementMethod = LinkMovementMethod.getInstance()
        } else {
            linearOwner.visibility = View.GONE
        }
        if (repo.license != null) {
            textRepoLicense.text = getLink(repo.license.url, repo.license.name)
            textRepoLicense.movementMethod = LinkMovementMethod.getInstance()
        } else {
            linearLicense.visibility = View.GONE
        }
    }

    private fun getLink(href: String, text: String): Spanned {
        val html = String.format("<a href=\"%s\">%s</a>", href, text)
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(html)
        }
    }
}