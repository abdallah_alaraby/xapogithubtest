package com.xapo.xapogithubtest.view.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.xapo.xapogithubtest.R
import com.xapo.xapogithubtest.common.view.FragmentLoader
import com.xapo.xapogithubtest.model.ApiError
import com.xapo.xapogithubtest.model.RepositoryItem
import com.xapo.xapogithubtest.view.adapter.RepositoriesAdapter
import com.xapo.xapogithubtest.viewmodel.RepositoriesViewModel
import kotlinx.android.synthetic.main.fragment_repositories.*

class RepositoriesFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener, RepositoriesAdapter.OnRepositoryClickListener {
    private var viewModel: RepositoriesViewModel? = null
    private var repositoriesAdapter = RepositoriesAdapter()

    companion object {
        fun newInstance(): RepositoriesFragment {
            return RepositoriesFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(RepositoriesViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_repositories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
        if (!viewModel?.isRequested!!) {
            requestRepositories(false)
            viewModel?.isRequested = true
        }
    }

    private fun initViews() {
        recyclerRepositories?.layoutManager = LinearLayoutManager(activity)
        recyclerRepositories?.adapter = repositoriesAdapter
    }

    private fun initObservers() {
        swipeRepositories?.setOnRefreshListener(this)
        swipeErrorView?.setOnRefreshListener(this)
        repositoriesAdapter.onRepositoryClickListener = this
        viewModel?.repositoriesListLiveData?.observe(this, Observer { setRepos(it) })
        viewModel?.errorLiveData?.observe(this, Observer { setError(it) })
    }

    private fun setRepos(repos: List<RepositoryItem>?) {
        swipeRepositories?.isRefreshing = false
        swipeErrorView?.isRefreshing = false
        repositoriesAdapter.setRepos(repos)
        repositoriesAdapter.notifyDataSetChanged()
    }

    private fun setError(apiError: ApiError?) {
        swipeRepositories?.isRefreshing = false
        swipeErrorView?.isRefreshing = false
        swipeErrorView?.visibility = View.VISIBLE
        swipeRepositories?.visibility = View.GONE
        when (apiError?.apiErrorType) {
            ApiError.ApiErrorType.API ->
                textError?.text = apiError.message
            ApiError.ApiErrorType.NETWORK ->
                textError?.text = getString(R.string.err_network_unreachable)
            ApiError.ApiErrorType.UNEXPECTED ->
                textError?.text = getString(R.string.err_unexpected)
        }
    }

    override fun onRefresh() {
        requestRepositories(true)
    }

    private fun requestRepositories(isRefreshing: Boolean) {
        swipeErrorView?.visibility = View.GONE
        swipeRepositories?.visibility = View.VISIBLE

        if (viewModel?.repositoriesListLiveData?.value == null
                || isRefreshing) {
            swipeRepositories?.isRefreshing = true
            swipeErrorView?.isRefreshing = true
            viewModel?.getRepositoriesList()
        }
    }

    override fun onRepositoryClick(repositoryItem: RepositoryItem) {
        FragmentLoader.loadFragment(activity?.supportFragmentManager!!,
                R.id.frame_fragment_container,
                RepositoryDetailsFragment.newInstance(repositoryItem),
                true)
    }
}