package com.xapo.xapogithubtest.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.xapo.xapogithubtest.R
import com.xapo.xapogithubtest.common.view.FragmentLoader
import com.xapo.xapogithubtest.view.fragment.RepositoriesFragment

class MainActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_FRAGMENT_LOADED = "fragment_loaded"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null || !savedInstanceState.getBoolean(EXTRA_FRAGMENT_LOADED, false)) {
            FragmentLoader.loadFragment(supportFragmentManager,
                    R.id.frame_fragment_container,
                    RepositoriesFragment.newInstance(),
                    false)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putBoolean(EXTRA_FRAGMENT_LOADED, true)
        super.onSaveInstanceState(outState)
    }
}