package com.xapo.xapogithubtest.common.view

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

class FragmentLoader {

    companion object {
        fun loadFragment(fragmentManager: FragmentManager,
                         @IdRes resourceId: Int,
                         fragment: Fragment,
                         addToBackStack: Boolean) {
            val ft = fragmentManager.beginTransaction()
                    .replace(resourceId, fragment)
            if (addToBackStack) {
                ft.addToBackStack(fragment::class.java.simpleName)
            }
            ft.commit()
        }
    }
}