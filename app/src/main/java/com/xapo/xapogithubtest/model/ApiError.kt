package com.xapo.xapogithubtest.model

data class ApiError(var apiErrorType: ApiErrorType, var message: String? = null) {

    enum class ApiErrorType {
        NETWORK, API, UNEXPECTED
    }
}