package com.xapo.xapogithubtest.model

import com.xapo.xapogithubtest.common.model.CloudServiceManager
import io.reactivex.Observable

class ListReposCloudImpl {

    fun getRepositories(query: String,
                        sort: String,
                        order: String): Observable<List<RepositoryItem>> {
        return CloudServiceManager
                .getClient()
                .create(ListReposCloud::class.java)
                .getRepositories(query, sort, order)
                .map { response -> response.items }
    }
}