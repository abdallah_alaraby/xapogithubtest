package com.xapo.xapogithubtest.model

import android.os.Parcel
import android.os.Parcelable

data class RepositoryLicense(val name: String, val url: String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RepositoryLicense> {
        override fun createFromParcel(parcel: Parcel): RepositoryLicense {
            return RepositoryLicense(parcel)
        }

        override fun newArray(size: Int): Array<RepositoryLicense?> {
            return arrayOfNulls(size)
        }
    }
}