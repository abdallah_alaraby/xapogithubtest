package com.xapo.xapogithubtest.model

data class RepositoriesResponse(val items: List<RepositoryItem>)