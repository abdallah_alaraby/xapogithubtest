package com.xapo.xapogithubtest.model

import android.os.Parcel
import android.os.Parcelable

data class RepositoryItem(val id: Long,
                          val name: String,
                          val owner: RepositoryOwner?,
                          val description: String,
                          val stargazers_count: Long,
                          val forks_count: Long,
                          val language: String,
                          val license: RepositoryLicense?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString(),
            parcel.readParcelable(RepositoryOwner::class.java.classLoader),
            parcel.readString(),
            parcel.readLong(),
            parcel.readLong(),
            parcel.readString(),
            parcel.readParcelable(RepositoryLicense::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeParcelable(owner, flags)
        parcel.writeString(description)
        parcel.writeLong(stargazers_count)
        parcel.writeLong(forks_count)
        parcel.writeString(language)
        parcel.writeParcelable(license, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RepositoryItem> {
        override fun createFromParcel(parcel: Parcel): RepositoryItem {
            return RepositoryItem(parcel)
        }

        override fun newArray(size: Int): Array<RepositoryItem?> {
            return arrayOfNulls(size)
        }
    }
}