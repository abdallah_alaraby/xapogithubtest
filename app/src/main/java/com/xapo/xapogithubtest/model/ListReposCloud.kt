package com.xapo.xapogithubtest.model

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ListReposCloud {

    @GET("search/repositories")
    fun getRepositories(@Query("q") query: String,
                        @Query("sort") sort: String,
                        @Query("order") order: String): Observable<RepositoriesResponse>
}